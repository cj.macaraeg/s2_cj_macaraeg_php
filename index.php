<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>s2: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
	<h1>Repetition Control Structure</h1>

	<h3>While Loop</h3>
	<?php whileLoop(); ?>
	<h3>Do-While Loop</h3>
	<?php doWhileLoop(); ?>
	<h3>For Loop</h3>
	<?php forLoop(); ?>
	<h3>Modified For Loops</h3>
	<?php modifiedForLoop(); ?>

	<h3>Associative Array</h3>
	<?php echo $gradePeriods['secondGrading']; ?>
	<ul>
		<?php foreach($gradePeriods as $period => $grade){?>
			<li>Grade in <?= $period ?> is <?= $grade ?> </li>
		<?php } ?> 
	</ul>


	<h3>Two-Dimensional Array</h3>
	<?php echo $heroes[2][1]; ?>

	<ul>
		<?php 
			foreach($heroes as $team){
				forEach($team as $member){
					?>
					<li><?php echo $member ?></li>
		<?php
				}
			}
		?>
	</ul>

	<h3>Sorting</h3>
	<pre>
	<?php print_r($sortedBrands); ?>
	</pre>

	<h3>Reverse Sorting</h3>
	<pre>
	<?php print_r($reverseSortedBrands); ?>
	</pre>

	<h2>Array Mutations (Append)</h2>
	<h3>push</h3>
	<?php array_push($computerBrands, 'Apple') ?>
	<pre>
	<?php print_r($computerBrands); ?>
	</pre>

	<h3>unshift</h3>
	<?php array_unshift($computerBrands, 'Dell') ?>
	<pre>
	<?php print_r($computerBrands); ?>
	</pre>

	<h2>Remove</h2>
	<?php array_pop($computerBrands) ?>
	<pre>
	<?php print_r($computerBrands); ?>
	</pre>

	<?php array_shift($computerBrands) ?>
	<pre>
	<?php print_r($computerBrands); ?>
	</pre>

	<h2>Count</h2>
	<pre>
	<?php echo count($computerBrands); ?>
	</pre>

	<p>
		<?php echo searchBrand($computerBrands, 'Asus') ?>
	</p>

	<h1>Activity 1</h1>

	<p>---------------------------------</p>

	<h3>Divisibles of Five</h3>
	
		<?php activity1(); ?>

	<h1>Activity 2</h1>

	<p>---------------------------------</p>

	<h2>Activity</h2>

	<h3>Array Manipulation</h3>
	
		<?php array_push($students, 'John Smith') ?>

			<p>
				<?php print_r($students); ?>	
			</p>

			<p>
				<?php echo count($students);?>
			</p>

		<?php array_push($students, 'Jane Smith') ?>

			<p>
				<?php print_r($students); ?>	
			</p>

			<p>
				<?php echo count($students);?>
			</p>

		<?php array_shift($students); ?>

			<p>
				<?php print_r($students); ?>
			</p>

			<p>
				<?php echo count($students);?>
			</p>


<body>
</html>